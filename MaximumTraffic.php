<?php

/**
 * The function determines the maximum traffic for every city in the representative provided graph
 *
 * @param array $strArr Is an array that contains the representation of an undirected graph
 * @return string Comma separated string in the format: "city:max_traffic,city:max_traffic..."
 */
function CityTraffic(array $strArr): string
{
    $graph = buildGraph($strArr);

    $maxTraffic = calculateMaxTraffic($graph);

    $result = formatResult($maxTraffic);

    return implode(',', $result);
}

/**
 * Build an adjacency list graph representation from the provided city information
 *
 * @param array $strArr An array of city information
 * @return array An adjacency list graph representation
 */
function buildGraph(array $strArr): array
{
    $graph = [];

    foreach ($strArr as $cityInfo) {
        // Regex helps to separate the city and his neighbors, [0] full item, [1] city, [2] neighbors
        preg_match('/(\d+):\[(\d+(?:,\d+)*)\]/', $cityInfo, $matches);
        $city = intval($matches[1]);
        $neighbors = array_map('intval', explode(',', $matches[2]));
        $graph[$city] = $neighbors;
    }

    return $graph;
}

/**
 * Calculate the maximum traffic for each city using DFS principles: https://en.wikipedia.org/wiki/Depth-first_search
 *
 * @param array $graph An adjacency list graph representation
 * @return array An array containing maximum traffic values for each city
 */
function calculateMaxTraffic(array $graph): array
{
    $maxTraffic = [];

    foreach ($graph as $city => $neighbors) {
        foreach ($neighbors as $neighbor) {
            // Reset visited cities for each road
            $visited = [];
            // Skip population from the current city been evaluated
            $visited[$city] = true;
            // Call to recursive calculation
            $maxTraffic[$city][$neighbor] = calculateMaxTrafficDFS($graph, $neighbor, $visited);
        }
    }

    return $maxTraffic;
}

/**
 * It is used as a recursive function that goes through all the neighboring cities of each city,
 * adding the population of each of them.
 *
 * @param array $graph Is an array that contains the representation of an undirected graph
 * @param int $city Current city been evaluated
 * @param array $visited Is a reference array that contains all the cities that has been already visited
 * @return int Recursive sum of cities
 */
function calculateMaxTrafficDFS(array $graph, int $city, array &$visited): int
{
    // Base case / Stop condition
    if (isset($visited[$city])) {
        return 0;
    }

    // Mark the city as visited
    $visited[$city] = true;
    $traffic = $city;

    // Visit its neighbors recursively
    foreach ($graph[$city] as $neighbor) {
        $traffic += calculateMaxTrafficDFS($graph, $neighbor, $visited);
    }

    return $traffic;
}

/**
 * Sort and format the maximum traffic results
 *
 * @param array $maxTraffic An array containing all traffic values for each city
 * @return array An array of formatted results
 */
function formatResult(array $maxTraffic): array
{
    ksort($maxTraffic);
    $result = [];

    foreach ($maxTraffic as $city => $traffic) {
        // Select the maximum traffic that would occur via a single road
        $result[] = "$city:" . max($traffic);
    }

    return $result;
}

// In order to run the example uncomment this block
/*
$input = ["1:[5]", "4:[5]", "3:[5]", "5:[1,4,3,2]", "2:[5,15,7]", "7:[2,8]", "8:[7,38]", "15:[2]", "38:[8]"];
$output = CityTraffic($input);
echo $output;
*/