### Installation

1. Clone the repo

```sh
git clone https://gitlab.com/avazquez0191/max-traffic-algorithm
```

### How to run

* Uncomment the example code at the file bottom
* Replace the example `$input`
* Check for results in console

```sh
cd ./max-traffic-algorithm
php -r "require 'MaximumTraffic.php';"
```